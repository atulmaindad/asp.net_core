﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ToDoApp.Data;



namespace ToDoApp.Models
{
    public static class DbInitializer
    {
        public static void Initialize(ToDoContext context)
        {
            context.Database.EnsureCreated();

            var todos = new ToDo();
            todos.Title = "First ToDo";
            todos.IsCompleted = true;
            context.ToDos.Add(todos);

            context.SaveChanges();
        }
    }
}
